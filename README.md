<!--
SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# Uncharted Waters Ahead

## Moving Legacy Software Infrastructure to Kubernetes

Talk held at [deRSE24 - Conference for Research Software Engineering in Germany](https://events.hifis.net/event/994/contributions/7926/).

### Abstract

Based on the precondition of the availability of Kubernetes as a Service, this talk is going to show why the migration of legacy software projects to a Kubernetes cluster is a worthwhile undertaking and how it can succeed, including the decisions made for the tools and the lessons learned on the way.

Starting from the current state with multiple deployment tools and environments (VMs, dedicated servers, Puppet, manual installs), we briefly cover the main problems arising from this historically grown structure.

We will then introduce a toolchain of CNCF Graduated software comprising Helm Charts as a standardized format for the deployment configuration of an application and ArgoCD as a deployment tool for continuously delivering applications to a Kubernetes cluster in a declarative and GitOps manner.

We will then see how this new toolchain for deployment and application hosting resolves or mitigates the aforementioned problems.
The second part of the talk will give a brief introduction of supporting services running inside the cluster that contribute to having a controlled, stable and easy to maintain environment for application deployments. This will cover possible solutions for certificate issuing, secrets management, logging and error tracking.
We will close the talk with an outlook on how a software development team could adapt to the new workflow and open a discussion on possible problems (e.g. limited resources) and coping strategies.
